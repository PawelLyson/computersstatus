#!/usr/bin/python3

import time
import psutil
import urllib.request
import urllib.error
import os
import math
import sys
import string
import random

from multiprocessing import Process

from googleapiclient.discovery import build
from google.oauth2 import service_account
import googleapiclient.discovery
import googleapiclient.http


class FileToUpload(object):
    def __init__(self, path_to_file=None, file_name=None, mime_type=None):
        self.__file_name = file_name
        self.__mime_type = mime_type
        self.__is_random = False
        self.__path_to_file = path_to_file

        if self.__path_to_file is None:
            self.__path_to_file = "random_" + self.id_generator() + '.dat'
            self.__create_random_file(name=self.__path_to_file)
            self.__is_random = True

        if self.__file_name is None:
            self.__file_name = os.path.splitext(os.path.split(self.__path_to_file)[1])[0]

        if self.__mime_type is None:
            self.__mime_type = "application/octet-stream"

    def __del__(self):
        if self.__is_random:
            #print("Delecting file: " + self.__file_name)
            os.remove(self.__path_to_file)

    def get_path_to_file(self):
        return self.__path_to_file

    def get_file_name(self):
        return self.__file_name

    def get_mime_type(self):
        return self.__mime_type

    def id_generator(self, size=6, chars=string.ascii_lowercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

    def __create_random_file(self, name, size=1024 * 1024 * 50):
        with open(name, "wb") as file_out:
            part_size = size//1024
            for i in range(1024):
                file_out.write(os.urandom(part_size))

        # print("Create file: " + name)


class UploadTest(object):
    def __init__(self, file_to_upload=None):
        module_direectory = os.path.dirname(__file__)
        self.__file_to_upload = file_to_upload
        if self.__file_to_upload is None:
            self.__file_to_upload = FileToUpload()
        if not isinstance(self.__file_to_upload, FileToUpload):
            raise AttributeError("Use FileToUpload class")
        scopes_drive = ['https://www.googleapis.com/auth/drive']
        service_account_credentials = os.path.join(module_direectory,"pythonspeedtest-c73b5b306a1e.json")
        self.__credentials = service_account.Credentials.from_service_account_file(service_account_credentials,
                                                                                   scopes=scopes_drive)
        self.__service = build('drive', 'v3', credentials=self.__credentials)
        self.__file_media_handler = self.__prepare_media()

        self.amount_checks = 24
        self.measuring_time = 5

    def get_file_list(self):
        results = self.__service.files().list(
            pageSize=100, fields="nextPageToken, files(id, name)").execute()
        items = results.get('files', [])

        if not items:
            print('No files found.')
        else:
            print('Files:')
            for item in items:
                print('{0} ({1})'.format(item['name'], item['id']))

    def __prepare_media(self):
        file_handler = open(self.__file_to_upload.get_path_to_file(), "rb")
        return googleapiclient.http.MediaIoBaseUpload(file_handler, mimetype=self.__file_to_upload.get_mime_type(),
                                                      chunksize=1024 * 1024, resumable=True)

    def __upload_media(self):
        task = self.__service.files().create(
            media_body=self.__file_media_handler,
            body={'name': self.__file_to_upload.get_file_name()})
        return task.execute()

    def __remove_media(self, fileId):
        self.__service.files().delete(fileId=fileId).execute()

    def __upload_process(self):
        while True:
            file_info = self.__upload_media()
            self.__remove_media(file_info['id'])

    def __measure_speed(self):
        measurements = []
        for empty in range(self.amount_checks):
            first_measure = psutil.net_io_counters(pernic=True)
            time.sleep(self.measuring_time)
            second_measure = psutil.net_io_counters(pernic=True)
            speed_in_bytes = (second_measure['eth0'][0] - first_measure['eth0'][0]) / self.measuring_time
            measurements.append(int('{:.0f}'.format(speed_in_bytes * 8)))

        measurements = sorted(measurements, reverse=True)[2:-2]
        elements_checked = math.ceil(len(measurements) * 25 / 100)
        average = sum(measurements[:elements_checked]) // elements_checked
        return average

    def start_test(self):
        process_data = Process(target=self.__upload_process)
        process_data.start()
        result = self.__measure_speed()
        process_data.terminate()

        return result


class DownloadTest(object):
    def __init__(self):
        self.__checked_URLs = self.load_urls()
        self.__download_test_processes = []
        self.endless_mode = False
        self.amount_checks = 24
        self.measuring_time = 5

    @staticmethod
    def load_urls():
        ok_urls = 0
        error_urls = 0
        checked_urls = []
        try:
            with open('urls.txt') as file:
                lines = file.read().splitlines()
                for url in lines:
                    if url.startswith('#'):
                        continue
                    try:
                        urllib.request.urlopen(url)
                    except (urllib.error.URLError, urllib.error.URLError) as error:
                        print("Checking URL Error " + str(error))
                        error_urls += 1
                    else:
                        checked_urls.append(url)
                        ok_urls += 1
            #print("Checking URLs:")
            #print("OK:" + str(ok_urls))
            #print("Bad:" + str(error_urls))
        except FileNotFoundError as error:
            print(str(error))
            sys.exit(2)

        if not checked_urls:
            print("[Error] Not enough correct urls")

        return checked_urls

    def __utilization_process(self, url):
        while True:
            urllib.request.urlretrieve(url, os.devnull)
            if not self.endless_mode:
                break

    def __prepare_utilization(self):
        self.__download_test_processes = []
        processor_counter = psutil.cpu_count()
        for i in range(processor_counter * 2):
            self.__download_test_processes.append(Process(target=self.__utilization_process,
                                                          args=(self.__checked_URLs[i % len(self.__checked_URLs)],)))

    def __measure_speed(self):
        measurements = []
        self.endless_mode = False
        for empty in range(self.amount_checks):
            first_measure = psutil.net_io_counters(pernic=True)
            time.sleep(self.measuring_time)
            second_measure = psutil.net_io_counters(pernic=True)
            speed_in_bytes = (second_measure['eth0'][1] - first_measure['eth0'][1]) / self.measuring_time
            measurements.append(int('{:.0f}'.format(speed_in_bytes * 8)))

        measurements = sorted(measurements, reverse=True)[2:-2]
        elements_checked = math.ceil(len(measurements) * 25 / 100)
        average = sum(measurements[:elements_checked]) // elements_checked
        return average

    def start_utilization(self):
        self.endless_mode = True
        for proc in self.__download_test_processes:
            proc.start()

    def start_test(self):
        self.__prepare_utilization()
        for process in self.__download_test_processes:
            process.start()
        speed_result = self.__measure_speed()

        for process in self.__download_test_processes:
            process.terminate()

        return speed_result


if __name__ == "__main__":
    print("Download:")
    download = DownloadTest()
    print(download.start_test())
    print("Upload:")
    upload = UploadTest()
    print(upload.start_test())
