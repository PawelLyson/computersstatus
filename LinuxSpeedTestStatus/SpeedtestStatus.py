from influxdb import InfluxDBClient
import subprocess
import time
import LinuxSpeedTestStatus.Speedtest as SpeedTest


client_connection = InfluxDBClient('localhost', 8086, 'root', 'root', "SystemStats")
Hostname = "Banan"


def main():
    # howMany = 0
    is_connected = False
    while not is_connected:
        trying = 0
        if trying == 60:
            exit(1)
        try:
            client_connection.get_list_database()
            is_connected = True
        except:
            time.sleep(10)
            trying += 1
    speed_test()


def speed_test():
    download_test = SpeedTest.DownloadTest()
    download_result = download_test.start_test()

    upload_test = SpeedTest.UploadTest()
    upload_result = upload_test.start_test()
    actual_time = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime())

    json_body = [
        {
            "measurement": "speedtest",
            "tags": {
                "host": Hostname,
                "type": "download"
            },
            "time": actual_time,
            "fields": {
                "value": download_result
            }
        },
        {
            "measurement": "speedtest",
            "tags": {
                "host": Hostname,
                "type": "upload"
            },
            "time": actual_time,
            "fields": {
                "value": upload_result
            }
        }]
    client_connection.write_points(json_body)


if __name__ == "__main__":
    main()

