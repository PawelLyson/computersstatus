#!/usr/bin/python3

from influxdb import InfluxDBClient
import time
import psutil

hostname = "Banan"

def main():
    client = InfluxDBClient('localhost', 8086, 'root', 'root', "SystemStats")
    #howMany = 0
    is_connected = False
    while not is_connected:
        trying = 0
        if trying == 60:
            exit(1)
        try:
            client.get_list_database()
            is_connected = True
        except:
            time.sleep(10)
            trying += 1

    print("[SystemStats] Start Measuring")
    while True:
        eth_upload_start = psutil.net_io_counters(pernic=True)["eth0"][0]
        eth_download_start = psutil.net_io_counters(pernic=True)["eth0"][1]
        tun_upload_start = psutil.net_io_counters(pernic=True)["tun0"][0]
        tun_download_start = psutil.net_io_counters(pernic=True)["tun0"][1]

        cpu_precentage_per_cpu = psutil.cpu_percent(interval=15, percpu=True)# oczekiwanie 15 sekund na średni pomiar

        eth_upload = (psutil.net_io_counters(pernic=True)["eth0"][0] - eth_upload_start)/15
        eth_download = (psutil.net_io_counters(pernic=True)["eth0"][1] - eth_download_start)/15
        tun_upload = (psutil.net_io_counters(pernic=True)["tun0"][0] - tun_upload_start)/15
        tun_download = (psutil.net_io_counters(pernic=True)["tun0"][1] - tun_download_start)/15

        memory_stats = psutil.virtual_memory()
        storage_stats = psutil.disk_usage('/')

        temp_sensors = psutil.sensors_temperatures()['iio_hwmon'][0][1] #linux only
        running_processes = psutil.pids().__len__()
        actual_time = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime())

        json_body = [
            {
                "measurement": "cpu_load_percent",
                "tags": {
                    "host": hostname,
                    "core": "Core 1"
                },
                "time": actual_time,
                "fields": {
                    "value": cpu_precentage_per_cpu[0]
                }
            },
            {
                "measurement": "cpu_load_percent",
                "tags": {
                    "host": hostname,
                    "core": "Core 2"
                },
                "time": actual_time,
                "fields": {
                    "value": cpu_precentage_per_cpu[1]
                }
            },
            {
                "measurement": "memory_usage",
                "tags": {
                    "host": hostname,
                    "type": "Used"
                },
                "time": actual_time,
                "fields": {
                    "value": (memory_stats[0]-memory_stats[1])
                }
            },
            {
                "measurement": "memory_usage",
                "tags": {
                    "host": hostname,
                    "type": "Free"
                },
                "time": actual_time,
                "fields": {
                    "value": memory_stats[1]
                }
            },
            {
                "measurement": "storage_usage",
                "tags": {
                    "host": hostname,
                    "type": "Used"
                },
                "time": actual_time,
                "fields": {
                    "value": storage_stats[1]
                }
            },
            {
                "measurement": "storage_usage",
                "tags": {
                    "host": hostname,
                    "type": "Free"
                },
                "time": actual_time,
                "fields": {
                    "value": storage_stats[2]
                }
            },
            {
                "measurement": "temperarure",
                "tags": {
                    "host": hostname,
                    "type": "CPU"
                },
                "time": actual_time,
                "fields": {
                    "value": temp_sensors
                }
            },
            {
                "measurement": "running_processes",
                "tags": {
                    "host": hostname
                },
                "time": actual_time,
                "fields": {
                    "value": running_processes
                }
            },
            {
                "measurement": "network_stats",
                "tags": {
                    "host": hostname,
                    "type": "Upload",
                    "iface": "eth0"
                },
                "time": actual_time,
                "fields": {
                    "value": eth_upload
                }
            },
            {
                "measurement": "network_stats",
                "tags": {
                    "host": hostname,
                    "type": "Download",
                    "iface": "eth0"
                },
                "time": actual_time,
                "fields": {
                    "value": eth_download
                }
            },
            {
                "measurement": "network_stats",
                "tags": {
                    "host": hostname,
                    "type": "Upload",
                    "iface": "tun0"
                },
                "time": actual_time,
                "fields": {
                    "value": tun_upload
                }
            },
            {
                "measurement": "network_stats",
                "tags": {
                    "host": hostname,
                    "type": "Download",
                    "iface": "tun0"
                },
                "time": actual_time,
                "fields": {
                    "value": tun_download
                }
            }
        ]

        client.write_points(json_body)
        #print(howMany)
        #howMany += 1
    client.close()

main()
