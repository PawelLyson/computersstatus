## ComputersStatus

---

*** LinuxStatus
Pobiera następujące dane z systemu linux oraz wysyła je do bazy danych influx:

1. Średnią prędkość z 15 sekund pobierania i wysyłania danych przez interfejsy eth0 i tun0
2. Średnie obciążenie procesora przez 15 sekund
3. Użycie pamięci ram
4. Użycie głównej pamięci (/)
6. Temperatura procesora
5. Ilość uruchomionych procesów

Dane są wysyłane cyklicznie co 15 sekund(w pętli nieskończonej) 

---

*** LinuxSpeedTestStatus

Wykonywanie testu prędkości oraz zwracanie aktualnej wartości pobierania oraz wysyłania.
Test pobierania jest wykonany poprzez pobieranie danych z zewnętrznych serwerów(serwery powinny być nasze lub pobieranie z serwerów powinno być za zgodą właściciela).
Test wysyłania jest wykonywany poprzez wysyłanie danych do specjalnego konta Google Drive dla maszyn(robotów). W tym celu użyto Google API Client oraz Google Oauth2.

Test pobierania oraz wysyłania jest wykonywany przez 60 sekund(sumarycznie 120 sekund).
